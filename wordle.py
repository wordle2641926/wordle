import requests as rq
import random

DEBUG = False

class WordleBot:
    words = [word.strip() for word in open("words.txt")]
    wordle_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = wordle_url + "register"
    creat_url = wordle_url + "create"
    guess_url = wordle_url + "guess"

    def __init__(self, name: str):
        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(WordleBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(WordleBot.creat_url, json=creat_dict)

        self.choices = WordleBot.words[:]
        random.shuffle(self.choices)

    def play(self):
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(WordleBot.guess_url, json=guess)
            rj = response.json()
            feedback = rj["feedback"]
            status = "win" in rj["message"]
            return feedback, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']
        attempts = 1

        while not won and attempts < 6:
            if DEBUG:
                print(choice, feedback, self.choices[:10])
            self.update(choice, feedback)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
            attempts += 1
        
        if won:
            print("Secret is", choice, "found in", attempts, "attempts")
            print("Route is:", " => ".join(tries))
        else:
            print("Failed to guess the word within 6 attempts.")
            print("Route is:", " => ".join(tries))

    def update(self, choice: str, feedback: str):
        def is_valid_choice(word: str, choice: str, feedback: str) -> bool:
            for i in range(5):
                if feedback[i] == 'g' and word[i] != choice[i]:
                    return False
                if feedback[i] == 'y' and (word[i] == choice[i] or choice[i] not in word):
                    return False
                if feedback[i] == 'b' and choice[i] in word:
                    return False
            return True
        
        self.choices = [w for w in self.choices if is_valid_choice(w, choice, feedback)]



game = WordleBot("sudha")
game.play()
